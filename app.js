//initialize express
var express = require('express');
var app = express();

//connect to the database
var database = require('./module/database');

//the port of the server
var port = 3000;

// Parse application/json
var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

//set the default router
var routes = require ('./routes/default');
app.use('/api', routes);

//let the server listen on port 3000
app.listen(port, function () {
   console.log('Lisening on port ' + port + '!');
});