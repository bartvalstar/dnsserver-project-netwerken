//Require express and the device schema
var express = require('express');
var Device = require('../schemas/device');

//Initialize the router
var router = express.Router();

//The default response
router.get('/', function (req,res) {
    res.status(200).send();
});

//The post request to put the device in de database
router.post('/add',function (req, res) {
    Device.findOne({publicKey: req.body.publicKey},function (err,foundDevice) {
        if (err) throw err;

        if(!foundDevice) {
            //Execute when a device isn't already in the database

            var name = req.body.name;
            var publicKey = req.body.publicKey;
            var address = req.body.address;

            var device = new Device({
                name: name,
                publicKey: publicKey,
                address: address
            });

            //Save the device to the database
            device.save(function (err) {
                if (err != null) {
                    //Send a "no content" status code when the request in incorrect
                    return res.status(400).send();
                } else {
                    //Send a "created" status code when te device is inserted in the database
                    return res.status(201).send();
                }
            });
        }else{
            //Execute when a device is already in the database

            foundDevice.update({created: Date.now()},function (err, result) {
                if(err){
                    //Send a "no content" status code when the request in incorrect
                    return res.status(400).send();
                } else if(result){
                    //Send a "created" status code when te device date is updated
                    return res.status(201).send();
                }
            })

        }
    });
});

//the get request to get a list of all the devices
router.get('/get', function (req, res) {
    Device.find({},{_id : 0, __v : 0, created : 0},function (err, devices) {
        //Send the list of devices in json format
        res.status(202).send(devices);
    });
});

//Export as a router
module.exports = router;
