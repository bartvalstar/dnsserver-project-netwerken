
//Initialize mongoose
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
mongoose.Promise = require('bluebird');

//Creat the device schema
var deviceSchema = new Schema({
    name: {type: String, required: true},
    publicKey: {type: String, required: true},
    address: {type: String, required: true},
    created: {type: Date, default: Date.now}
});

//export the model3
module.exports = mongoose.model("Device",deviceSchema);