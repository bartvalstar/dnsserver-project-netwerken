//Require mongoose
var mongoose = require('mongoose');
mongoose.Promise = require('bluebird');

//Connect to the database
mongoose.connect('mongodb://localhost/dnsserver');

//Initialize connection
var database = mongoose.connection;
database.on('error', console.error.bind(console, 'connection error:'));
database.once('open', function() {
    //Database is connected!

    //clean database
    database.collection('devices').drop();

    //Set a expire time on the documents
    database.collection('devices').ensureIndex({'created': 1}, {expireAfterSeconds: 120 });
    console.log('connected to the database')
});
